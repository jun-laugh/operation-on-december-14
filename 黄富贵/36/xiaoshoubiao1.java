package xiaoshoubiao;

import java.util.Scanner;
//家贸易公司有4位销售员，每位销售员负责销售相同的4种商品
//使用二维数组，编写一个程序，接收每名销售员销售的各种产品的数量。 打印产品销售明细表
//明细表包括：每类产品的销售总数，以及每位销售员销售的产品数量占总销售的百分比。
public class xiaoshoubiao1 {
public static void main(String[] args) {
	Scanner sm = new Scanner(System.in);
	double[][] shangpin =new double[4][4];
	double zongshu = 0;
	for (int i = 0; i < shangpin.length; i++) {
		System.out.println("请输入商品"+(i+1)+"的销售1的量:");
		shangpin[i][0]=sm.nextDouble();
		System.out.println("请输入商品"+(i+1)+"的销售2的量:");
		shangpin[i][1]=sm.nextDouble();
		System.out.println("请输入商品"+(i+1)+"的销售3的量:");
		shangpin[i][2]=sm.nextDouble();
		System.out.println("请输入商品"+(i+1)+"的销售4的量:");
		shangpin[i][3]=sm.nextDouble();
	}
	System.out.println("-----销售表-----");
	System.out.println("销售1\t销售2\t销售3\t销售4\t销售总数\t百分比");
	for (int i = 0; i < shangpin.length; i++) {
		zongshu=shangpin[i][0]+shangpin[i][1]+shangpin[i][2]+shangpin[i][3];
		System.out.println(shangpin[i][0]+
				"\t"+shangpin[i][1]+
				"\t"+shangpin[i][2]+
				"\t"+shangpin[i][3]+
				"\t"+zongshu+"\t"+(int)((double)shangpin[i][0]/zongshu*100)+"%");
	}
}
}
